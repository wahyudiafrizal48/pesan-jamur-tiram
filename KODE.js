import { Text, StyleSheet, View, TouchableOpacity, TextInput, Alert, ImageBackground } from 'react-native'
import React, { Component } from 'react'
import jamur from './foto/jamur.jpg'

export default class extends Component {
  constructor(props) {
    super(props)

    this.state = {
      Kode: '',
      islogin: false
    }
  }

  login = () => {

    if (!this.state.Kode)
      Alert.alert("Error", " kode verifikasi harus diisi")

    else (
      this.setState({
        islogin: true
      })
    )
  }

  render() {
    const { Kode, islogin } = this.state;
    return (
      <ImageBackground source={jamur} style={{ flex: 1 }}>
        <View style={styles.putih}>
          <View style={styles.satu}>
            <Text style={styles.teks}>Silahkan Masukkan kode Verifikasi</Text>

            {/*isi*/}
            <View style={styles.Kode}>
              <TextInput
                style={styles.TextInput}
                placeholder='Masukkan Kode'
                value={Kode}
                onChangeText={(Kode) => this.setState({ Kode })}
                secureTextEntry={true}
              />
            </View>

            <TouchableOpacity style={styles.tombollogin} onPress={() => this.login()}>
              <Text style={styles.login}>
                Kirim
              </Text>
            </TouchableOpacity>

            {islogin && (
              <Text style={{ marginTop: 8, color: 'red', fontSize: 15 }}>
                Kode salah
              </Text>
            )}
          </View>
        </View>
      </ImageBackground>


    )
  }
}

const styles = StyleSheet.create({
  putih: {
    justifyContent: 'center', marginHorizontal: 20, marginVertical: 200,
    borderRadius: 10
  },
  satu: { padding: 20 },
  teks: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black'
  },
  isi: {
    marginTop: 10
  },

  TextInput: {
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 10,
    padding: 10,
    borderColor: 'black',
    height: 40,
    backgroundColor: '#F0EBE3'
  },
  password: {
    marginTop: 10,
  },
  tombollogin: {
    marginTop: 20,
    alignItems: 'center',

  },

  login: {
    borderRadius: 5,
    width: 100,
    textAlign: 'center',
    padding: 10,
    color: 'black',
    fontWeight: 'bold',
    borderWidth: 1,
    backgroundColor: '#BF9742'
  }
})