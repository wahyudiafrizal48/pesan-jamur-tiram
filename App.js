import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";

import LOGIN from "./src/page/home/LOGIN";
import KODE from "./src/router/KODE";

const wahyudi = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <wahyudi.Navigator initialRouteName='beranda'>
        <wahyudi.Screen name='LOGIN NOMER' component={LOGIN} />
        <wahyudi.Screen name='KODE VERIFIKASI' component={KODE} />
      </wahyudi.Navigator>
    </NavigationContainer>
  );
}

export default App;