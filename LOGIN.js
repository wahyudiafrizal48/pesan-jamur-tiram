import { Text, View, TouchableOpacity, TextInput, ImageBackground } from 'react-native'
import React, { useState, useEffect } from 'react';
import jamur from './foto/jamur.jpg'

const coba = ({ navigation }) => {
    return (
        <ImageBackground source={jamur} style={{ flex: 1, padding: 20 }}>

            <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 200, textAlign: 'center', color: 'black' }}>
                MASUKKAN NOMER ANDA
            </Text>

            {/*isi*/}
            <View style={{ marginTop: 10 }}>
                <Text style={{ color: 'black', textAlign: 'center' }}>
                    kami akan mengirim kode verifikasi setelah anda memasukkan nomer
                </Text>
                <TextInput
                    style={{
                        borderWidth: 1,
                        borderRadius: 5,
                        marginTop: 20,
                        padding: 10,
                        borderColor: 'black',
                        height: 40,
                        backgroundColor: '#F0EBE3',
                        marginStart: 10,
                        marginEnd: 10
                    }}
                    placeholder='Nomer Anda'
                />
            </View>

            <TouchableOpacity
                style={{
                    borderRadius: 5,
                    width: 100,
                    textAlign: 'center',
                    padding: 10,
                    color: 'black',
                    fontWeight: 'bold',
                    borderWidth: 1,
                    backgroundColor: '#BF9742',
                    marginTop: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: 130
                }}
                onPress={() => navigation.navigate('KODE VERIFIKASI')}>
                <Text style={{
                    fontSize: 15, fontWeight: 'bold',
                    color: 'black',
                    textAlign: 'center', alignItems: 'center'
                }}>
                    Kirim
                </Text>
            </TouchableOpacity>
        </ImageBackground>
    )
}


export default coba;

